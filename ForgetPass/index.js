function validateEmail(email) {
    const reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return reg.test(String(email).toLowerCase());
}

let resetPassBtn = document.querySelector('.forgetPassSubmit');

resetPassBtn.addEventListener('submit', (event) => {
    
    let mailInput = document.querySelector('.forgetPassInput');

    if(validateEmail(mailInput.value)) {
        alert("Qeyd olunmuş mail ünvanına təlimat göndərildi!");
        mailInput.value = "";
    }
    else if (mailInput.value == "") {
        alert("Zəhmət olmasa mail ünvanınızı daxil edin!");
    }
    else {
        alert("Daxil olunmuş mail ünvanında problem var!");
    }
});